import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {


   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   private double real;
   private double i;
   private double j;
   private double k;
   private static final double equalsConst = 0.000000001;

   public Quaternion (double a, double b, double c, double d) {
      real = a;
      i = b;
      j = c;
      k = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return real;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return i;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return j;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return k;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String[] addons  = new String[]{"", "i", "j", "k"};
      double[] arr = new double[]{real, i, j, k};
      StringBuilder result = new StringBuilder();
      for (int l = 0; l < arr.length; l++) {
         int a = (int) arr[l];
         if (a > 0 && l > 0){
            result.append("+").append(a).append(addons[l]);
            continue;
         } else if (a == 0){
            continue;
         } else{
            result.append(a).append(addons[l]);
         }
      }
      return result.toString();
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    * Regex matches: https://stackoverflow.com/questions/6020384/create-array-of-regex-matches
    */
   public static Quaternion valueOf (String s) {
      List<String> allMatches = new ArrayList<>();
      Matcher m = Pattern.compile("[-]?\\d+[ijk]?").matcher(s);
      while (m.find()){
         allMatches.add(m.group());
      }
      double real = 0;
      double i = 0;
      double j = 0;
      double k = 0;
      for (int x = 0; x < allMatches.size(); x++) {
         if(x==0){
            real = Double.parseDouble(allMatches.get(x));
         }
         if(allMatches.get(x).contains("i") && x == 1){
            i = Double.parseDouble(allMatches.get(x).replaceAll("[^\\d-]", ""));
         }
         if(allMatches.get(x).contains("k")){
            k = Double.parseDouble(allMatches.get(x).replaceAll("[^\\d-]", ""));
         }
         if(allMatches.get(x).contains("j")){
            j = Double.parseDouble(allMatches.get(x).replaceAll("[^\\d-]", ""));
         }
      }
      if (!new Quaternion(real, i, j, k).toString().equals(s)){
         throw new RuntimeException("Wrong input string " + s);
      }
      return new Quaternion(real, i, j, k);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(real, i, j, k);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(real) < equalsConst && Math.abs(i) < equalsConst && Math.abs(j) < equalsConst && Math.abs(k) < equalsConst;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      double b = -i;
      double c = -j;
      double d = -k;
      return new Quaternion(real, b, c, d);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      double r = -real;
      double b = -i;
      double c = -j;
      double d= -k;
      return new Quaternion(r, b, c, d);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      double r = real + q.getRpart();
      double b = i + q.getIpart();
      double c = j + q.getJpart();
      double d = k + q.getKpart();
      return new Quaternion(r, b, c, d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double r = real*q.getRpart()-i*q.getIpart()-j*q.getJpart()-k*q.getKpart();
      double b = real*q.getIpart()+i*q.getRpart()+j*q.getKpart()-k*q.getJpart();
      double c = real*q.getJpart()-i*q.getKpart()+j*q.getRpart()+k*q.getIpart();
      double d = real*q.getKpart()+i*q.getJpart()-j*q.getIpart()+k*q.getRpart();
      return new Quaternion(r, b, c, d);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      double x =real * r;
      double b = i * r;
      double c = j * r;
      double d = k * r;
      return new Quaternion(x, b, c, d);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if(isZero()){
         throw new IllegalArgumentException("Division by 0");
      }
      double r = real / (real*real + i*i + j*j + k*k);
      double b = -i / (real*real + i*i + j*j + k*k);
      double c = -j / (real*real + i*i + j*j + k*k);
      double d = -k / (real*real + i*i + j*j + k*k);
      return new Quaternion(r, b, c, d);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if(q.isZero()){
         throw new IllegalArgumentException("Division by 0");
      }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if(q.isZero()){
         throw new IllegalArgumentException("Division by 0");
      }
      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      Quaternion other = (Quaternion) qo;
      return Math.abs(getKpart() - other.getKpart()) < equalsConst &&
              Math.abs(getJpart() - other.getJpart()) < equalsConst &&
              Math.abs(getIpart() - other.getIpart()) < equalsConst &&
              Math.abs(getRpart() - other.getRpart()) < equalsConst;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return times(q.conjugate()).plus(q.times(conjugate())).times(0.5);
   }


   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(real*real+i*i+j*j+k*k);
   }
   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(real, i, j, k);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion(0, 0, 0, 0);
      Quaternion arv2 = new Quaternion(-1, -2, 3, 0);
      System.out.println(valueOf("1+2+3+4"));
      System.out.println(valueOf(arv1.toString()));
   }
}
// end of file// end of file